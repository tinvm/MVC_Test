<?php
include_once ROOT_PATH . '/components/Controller.php';
include_once ROOT_PATH . '/model/Book.php';

/**
* BookController
*/
class BookController extends Controller
{
	public function indexAction() {
		$books = Book::findAll();

        $this->render("index", [
            'books' => $books
        ]);
	}

	public function viewAction() {
		if ( isset($_GET['id'] )) {
            $book = Book::find($_GET['id']);
            if ($book) {
                $this->render("view", [
                    'book'  => $book
                ]);
            } else {
                // throw error
            }
		} else {
			//redirect to home
		}
	}
}

?>