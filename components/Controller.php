<?php
include_once ROOT_PATH . "/components/App.php";
/**
 * Created by PhpStorm.
 * User: hoangdieu
 * Date: 2/12/16
 * Time: 10:36 PM
 */

class Controller {

    public function render ($file, $datas = []) {
        $files = explode("/", $file);
        if(count($files) == 1) {
            $controller = str_replace( "controller", "", strtolower(get_called_class()) );
            $path_file = $controller . "/" . $file;
        } else {
            $path_file = $file;
        }
        foreach ( $datas as $key => $value ) {
            $$key = $value;
        }
        ob_start();
        require_once ROOT_PATH . "/view/" . $path_file . ".php";
        $contents = ob_get_contents();
        ob_end_clean();

        include ROOT_PATH . "/themes/defaults/layouts/main.php";
    }

    public function redirect ($route,$params) {
        header("Location:" . App::url($route, $params) );
    }
}