<?php
/**
 * Created by PhpStorm.
 * User: hoangdieu
 * Date: 2/13/16
 * Time: 6:26 PM
 */

class App {
    public static function homeUrl () {
        $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') ."://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $url = explode("?",$root);
        return $url[0];
    }

    public static function url ($route, $params = []) {
        $routes = explode("/", $route);
        $url_params = [];
        if ( count($routes) == 1 ) {
            $url_params["controller"] = $route;
        } else {
            $url_params["controller"] = $routes[0];
            $url_params["action"] = $routes[1];
        }
        $url_params = array_merge($url_params, $params);
        $url = http_build_query($url_params);
        return static::homeUrl() . "?" . $url;
    }
}