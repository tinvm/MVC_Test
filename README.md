## Config project
First, you need create a database in you mysql. 

Import `data/mvc_test.sql` into it.

Modify file `config/db.php`,edit parameters with your mysql.

```
<?php
defined("DB_HOST") or define("DB_HOST", "localhost");
defined("DB_UNAME") or define("DB_UNAME", "root");
defined("DB_PASS") or define("DB_PASS", "root");
defined("DB_DBNAME") or define("DB_DBNAME", "mvc_test");
defined("DB_PORT") or define("DB_PORT", "3306");
?>
```


## Run project
- Goto `http://localhost/MVC_Test` (MVC_Test is project folder name)
- To view all book: `http://localhost/MVC_Test/?controller=book` or `http://localhost/MVC_Test/inex.php?controller=book`