<table class="table table-bordered table-responsive table-striped table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Content</th>
        <th>SubjectID</th>
        <th>Resource</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($books as $book) {
        ?>
        <tr>
            <td><?= $book->id ?></td>
            <td><?= substr($book->contents_vi, 0, 100) ?></td>
            <td><?= $book->subjectid ?></td>
            <td><?= $book->resource ?></td>
            <td>
                <a href="<?=App::url("book/view",['id'   => $book->id])?>" title="View" aria-label="View" data-pjax="0"><span
                        class="glyphicon glyphicon-eye-open"></span></a>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>