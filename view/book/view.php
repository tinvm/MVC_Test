<table class="table table-striped table-bordered detail-view">
	<tbody>
		<tr>
			<th>ID</th>
			<td><?=$book->id?></td>
		</tr>
		<tr>
			<th>SubjectID</th>
			<td><?=$book->subjectid?></td>
		</tr>
		<tr>
			<th>Content</th>
			<td><?=substr($book->contents_vi, 0, 500)?></td>
		</tr>
	</tbody>
</table>