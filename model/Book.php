<?php
include_once ROOT_PATH . '/model/BaseModel.php';
/**
* Book Model
*/
class Book extends BaseModel
{
	public $id;
	public $subjectid;
	public $contents_vi;
	public $contents_en;
	public $contents_ja;
	public $resource;

	public static function getTableName () {
		return "books";
	}
}
?>