<?php
/**
* Base Model
*/
class BaseModel
{
	/**
	 * tableName of Model
	*/
	public static function getTableName () {
		return "tableName";
	}

	public static function createCommand ($query) {
		$dns = "mysql:host=" . DB_HOST . ";dbname=" . DB_DBNAME; 
		$dns .= ";charset=utf8";
		$pdo = new PDO($dns, DB_UNAME, DB_PASS );
		return $pdo->query ($query);
	}

	public static function findAll() {
		try {
		    //connect as appropriate as above
		    $stmt = static::createCommand('SELECT * FROM ' . static::getTableName() . " LIMIT 20 ");
		    return static::createObjects($stmt->fetchAll(PDO::FETCH_OBJ), get_called_class());
		} catch(PDOException $ex) {
		    echo "An Error occured!";
		    throw $ex;
		}
	}

	public static function find ($id) {
		try {
		    //connect as appropriate as above
		    $stmt = static::createCommand('SELECT * FROM ' . static::getTableName() . " WHERE id = '" . $id. "'" );
		    return static::createObject($stmt->fetch(PDO::FETCH_OBJ), get_called_class());
		} catch(PDOException $ex) {
		    echo "An Error occured!";
		    throw $ex;
		}
	}

	public static function createObjects ($objects, $className) {
		$datas = [];
		foreach ( $objects as $object ) {
			$datas[] = static::createObject ($object, $className);
		}
		return $datas;
	}

	public static function createObject ($object, $className) {
		$obj = new $className();
		foreach (array_keys(get_class_vars($className)) as $value ) {
			if ( isset($object->$value) ) {
				$obj->$value = $object->$value;
			}
		}
		return $obj;
	}
	public function test () {
		echo get_called_class();
		var_dump(get_class_vars(get_called_class()));
		if (property_exists(get_called_class(), "id")) {
			echo "exist";
		}
	}
}
?>