<?php
defined('ROOT_PATH') or define('ROOT_PATH', __DIR__);
//include config
require_once ( ROOT_PATH . "/config/db.php" );

//include controller
foreach (glob("components/*.php") as $comp) {
    require_once $comp;
}

//include controller
foreach (glob("controller/*.php") as $controller_file) {
  require_once $controller_file;
}


if ( isset($_GET['controller']) ) {
	$classController = ucfirst($_GET['controller']) . "Controller"; //Tên Class Controller

	if ( isset($_GET["action"]) ) {
		$action = $_GET["action"] . "Action"; // name action function in controller
	} else {
		//default indexAction
		$action = "indexAction";
	}
	$controller = new $classController();
	$controller->$action();
} else {
	$classController = "SiteController"; 
	$controller = new $classController();
	$controller->indexAction();
}